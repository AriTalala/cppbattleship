#ifndef BATTLESHIPAI_H
#define BATTLESHIPAI_H

#include <QObject>
#include "battleshipscreen.h"

class BattleshipAi
{
public:
    BattleshipAi();



private:

    BattleshipScreen screen;
    std::vector<BattleshipScreenCoordinate*> bombedCoords;
    void bombLocation(int x, int y);
    void scanEnemyShip(int x, int y);
    void pickNextCoord();

};

#endif // BATTLESHIPAI_H
