#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QPainter"
#include "QRectF"
#include "QLabel"
#include "QLayout"
#include "QPushButton"
#include "QBoxLayout"

#include <QApplication>
#include <QScrollArea>
#include <QScreen>

#include "battleshipscreen.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      scrollarea(new QScrollArea())
{


    ui->setupUi(this);

    //this base is used to to setup and set a layout fitting for players battleship map and AI-controlled map

    QWidget *base = new QWidget(this);

    player_screen = new BattleshipScreen;

    ai_screen = new BattleshipScreen;

    grid = new QGridLayout(base);

    grid->addWidget(player_screen, 0, 0);

    grid->addWidget(ai_screen, 0, 2);

    base->setLayout(grid);

    this->setCentralWidget(base);





    /*
    player_screen->update();
    ai_screen->update();
    */




    //resize(QGuiApplication::primaryScreen()->availableSize() * 3 / 5);



}



MainWindow::~MainWindow()
{
    delete ui;
}

