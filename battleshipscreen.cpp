#include "battleshipscreen.h"
#include "battleshipscreencoordinate.h"

BattleshipScreen::BattleshipScreen()
{

    update();
}


void BattleshipScreen::paintEvent(QPaintEvent *event){



        QPainter painter(this);
        painter.setPen(QPen(Qt::black, 3, Qt::SolidLine, Qt::SquareCap));

        //Here we ought to draw a rect for map edges so that it can house 10*10 QWidgets that are the COORDINATESPOTS
        painter.drawRect(25,25,250,250);

        //these are the rects that are the squares ships are put to.
        //here we ought to place QWidgets called Squares to which touchEvent can be tied to.
        for (int x = 1; x < xSize+1; x++)
            for (int y = 1; y < ySize+1; y++){
                painter.drawRect(25*x,25*y,25,25);

                //mapGrid->addWidget(new BattleshipScreenCoordinate(),x,y);

                //coord.update();
            }


}
