#ifndef BATTLESHIPSCREEN_H
#define BATTLESHIPSCREEN_H

#include "QWidget"
#include "QPainterPath"
#include "QPaintEvent"
#include "QPainter"
#include "battleshipscreencoordinate.h"
#include <QGridLayout>


/*
This QWidget subclass represents the map which are the ship coordinates.
Coordinates themselves are shown as BattleshipScreenCoordinate QWidgets.

*/
class BattleshipScreen : public QWidget
{
public:
    BattleshipScreen();

    const int ySize = 10;
    const int xSize = 10;


    //location of a ship is stored here
    int map[10][10];

    void insertShip();
    void checkOverlap(int x, int y);

protected:
    void paintEvent(QPaintEvent *event) override;


};

#endif // BATTLESHIPSCREEN_H
