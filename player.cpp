#include "player.h"
#include <vector>

/*We have a human player and AI player. By this boolean we make a distinction*/
Player::Player(bool realPlayer)
{

    isRealPlayer = realPlayer;
}


void Player::initShiplist(){

ships.push_back(new Gunboat());
ships.push_back(new Destroyer());
ships.push_back(new Destroyer());
ships.push_back(new Cruiser());
ships.push_back(new Carrier());


}
