#include "battleshipscreencoordinate.h"
#include <QMouseEvent>


BattleshipScreenCoordinate::BattleshipScreenCoordinate(QWidget *parent) : QWidget(parent)
{

    update();

}

void BattleshipScreenCoordinate::paintEvent(QPaintEvent *event){



        QPainter painter(this);
        painter.setPen(QPen(Qt::black, 3, Qt::SolidLine, Qt::SquareCap));

        //Here we ought to draw a rect for map edges so that it can house 10*10 QWidgets that are the COORDINATESPOTS
        painter.drawRect(0,0,15,15);
        //QRect rect;
        //painter.drawRect(rect);




}

int BattleshipScreenCoordinate::getPositionOnScreen(int pos) {
  return pos / coordSize;
}


// Handle a button press.
void BattleshipScreenCoordinate::mousePressEvent(QMouseEvent *event) {
  // Check if we are in the map and convert to grid numbers.
  int x = getPositionOnScreen(event->x());
  int y = getPositionOnScreen(event->y());

}

// Handle a button release.
void BattleshipScreenCoordinate::mouseReleaseEvent(QMouseEvent *event) {
  this->update();

  // Check if we are in the map and convert to grid numbers.
  int x = getPositionOnScreen(event->x());
  int y = getPositionOnScreen(event->y());

}

// Handle mouse movement while a button is clicked.
void BattleshipScreenCoordinate::mouseMoveEvent(QMouseEvent *event) {
  // Check if we are in the map and convert to grid numbers.
  int x = getPositionOnScreen(event->x());
  int y = getPositionOnScreen(event->y());

  this->update();
}
