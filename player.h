#ifndef PLAYER_H
#define PLAYER_H

#include "battleship.h"
#include "battleshipscreen.h"
#include "gunboat.h"
#include "destroyer.h"
#include "cruiser.h"
#include "carrier.h"

#include <vector>


class Player
{
public:
    Player(bool realPlayer);
    bool isRealPlayer;

 protected:

    std::vector<Battleship*> ships;
    BattleshipScreen screen;
    void initShiplist();
};

#endif // PLAYER_H
