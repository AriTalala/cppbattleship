#ifndef DESTROYER_H
#define DESTROYER_H

#include "battleship.h"

class Destroyer : public Battleship
{
public:
    Destroyer();

    int length = 3;
};

#endif // DESTROYER_H
