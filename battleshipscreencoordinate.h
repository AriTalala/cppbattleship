#ifndef BATTLESHIPSCREENCOORDINATE_H
#define BATTLESHIPSCREENCOORDINATE_H

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>


class BattleshipScreenCoordinate : public QWidget
{
    Q_OBJECT
public:
    explicit BattleshipScreenCoordinate(QWidget *parent = nullptr);

    const int notHit = 0;
    const int isHit = 1;
    const int hasHitShip = 2;

    const int coordSize = 25;



    int status = 0;
    int x = 0;
    int y = 0;



protected:
    void paintEvent(QPaintEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    int getPositionOnScreen(int pos);

    void setStatus(int stat);
    int getStatus();
    void setX(int x);
    int getX();
    void setY(int y);
    int getY();


signals:

};

#endif // BATTLESHIPSCREENCOORDINATE_H
