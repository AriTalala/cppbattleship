#ifndef BATTLESHIP_H
#define BATTLESHIP_H

/*
This is a base class for all battleships which are;
Gunboat with 2 hp, that is length of 2 squares,
Destroyer with 3 hp, that is length of 3 squares,
Cruiser with 4 hp, that is length of 4 squares,
Carrier with 5 hp that is length of 5 squares.

These each have their subclasses in which we individually set length accordingly.

*/

class Battleship
{
public:
    Battleship();

    int x = 0;
    int y = 0;


protected:


protected:


    void setCoordinates(int x, int y);
    int getCoordinateX();
    int getCoordinateY();
};

#endif // BATTLESHIP_H
