#ifndef CRUISER_H
#define CRUISER_H

#include "battleship.h"

class Cruiser : public Battleship
{
public:
    Cruiser();

    int length = 4;
};

#endif // CRUISER_H
