#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "battleshipscreen.h"
#include "QPainter"
#include <QScrollArea>
#include <QGridLayout>
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    BattleshipScreen *player_screen;
    BattleshipScreen *ai_screen;


    QLabel *label;
    QScrollArea *scrollarea;
    QGridLayout *grid;

private:
    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
