#ifndef CARRIER_H
#define CARRIER_H

#include "battleship.h"


class Carrier : public Battleship
{
public:
    Carrier();

    int length = 5;
};

#endif // CARRIER_H
